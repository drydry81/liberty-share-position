const { gql } = require('apollo-server');

const typeDefs = gql`
    type Query {
        positions: [Position]
        position(sessionId: ID!): Position
    }
    
    type Position {
        sessionId: ID!
        latitude: String
        longitude: String
        couleur: Couleur
    }
    
    enum Couleur {
        Rouge
        Bleu
        Vert
    }
    
    type Mutation {
        # Update the position
        updatePosition(latitude: String, longitude: String, sessionId: ID, couleur: Couleur) : PositionUpdateResponse
    }
    
    type PositionUpdateResponse {
        success: Boolean!
        message: String
        position: Position
    }
`;

module.exports = typeDefs;