const admin = require('firebase-admin');
const _ = require('lodash');
const {ApolloError} = require('apollo-server-errors');

class PositionAPI {

    constructor() {
        this.ref = admin.database().ref('positions');
    }

    async getAllPositions() {
        return this.ref.once("value").then(function(snapshot) {
            const data = snapshot.val();

            let response = [];

            _.forOwn(data, (position, sessionId) => {
                response.push(position);
            });

            return response;
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
            return [];
        });
    }

    async getPositionById({ sessionId }) {
        return this.ref.child(sessionId).once('value').then(snapshot => {
                return snapshot.val();
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
            return [];
        });
    }

    async updatePosition({ position }) {
        try {
            const ref = this.ref.child(position.sessionId);
            await ref.orderByChild("sessionId").once("value",snapshot => {
                return snapshot.ref.set(position);
            });
        } catch (error) {
            throw new ApolloError(error);
        }
    }
}

module.exports = PositionAPI;