module.exports = {
    Query: {
        positions: async (_, __, { dataSources }) =>
            dataSources.positionAPI.getAllPositions(),
        position: async (_, { sessionId }, { dataSources }) =>
            dataSources.positionAPI.getPositionById({ sessionId: sessionId })
    },
    Mutation: {
        updatePosition: async (_, position, { dataSources } ) => {
            await dataSources.positionAPI.updatePosition({ position });
            return {
                success: true,
                message: 'Position successfully updated',
                position: position
            };
        }
    }
};