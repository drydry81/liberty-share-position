const { ApolloServer } = require('apollo-server');
const typeDefs = require('./schema');
const resolvers = require('./resolvers');
const admin = require('firebase-admin');
const serviceAccount = require('../project-49a77-firebase-adminsdk-2bp7c-021521d206.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://project-49a77.firebaseio.com/',
});

const PositionAPI = require('./datasources/position');

const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => ({
        positionAPI: new PositionAPI()
    })
});

server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});