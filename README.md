# liberty-share-position

## Points realises:
- Utilisation d'une base de donnees Firebase Real Time: tous les fichiers necessaires pour utuliser ma base de donnees sont inclus dans le projet
- Implementation d'une API GraphQL basee sur Apollo a base du schema, de resolvers et de datasources
- Implementation de quelques Queries: GetPositions (toutes les positions) et GetPositionById (trouver une position par sessionId)
- Implementation d'une mutation updatePosition: mise a jour de la position en base de donnees, formattage de la reponse

## Points restants:
- 0% - creer la route en front pour afficher une carte GMaps qui affiche en temps reel les points: manque de temps et d'experience Apollo
- 0% - creer la partie Dockerfile: manque de temps

## Comment lancer le projet:
```sh
$ npm install
$ npm start
```

## Structure des donees dans Firebase:
voir le fichier _data/project-49a77-export.json_ qui est un backup des donnees.

```json
{
  "positions" : {
    "-LaRiuRmLXDmaX3F7d8V" : {
      "couleur" : "Rouge",
      "sessionId" : "-LaRiuRmLXDmaX3F7d8V",
      "latitude" : "43.8857",
      "longitude" : "2.1697299999999586"
    },
    "-LaRj40mAuPmT-OnRJT7" : {
      "couleur" : "Bleu",
      "sessionId" : "-LaRj40mAuPmT-OnRJT7",
      "latitude" : "43.886170952679926",
      "longitude" : "2.1683086825712508"
    }
  }
}
```

